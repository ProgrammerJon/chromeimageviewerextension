var canvas;
var bUpdateCanvas = false;
var isViewingCurrentTab = false;
// Mouse
var mouseDeltaX = 0;
var mouseDeltaY = 0;
var targetDeltaX = 0; // targetDelta is the lerped version of mouseDelta that is fed into the relativePan function
var targetDeltaY = 0;
var lerpFactor = 0.15;
var mouseLastPoint = null;
var currentMousePoint = null;
var isDragging = false;
// Image
var lastATanAngle = 0;
var imageAngle = 0;
var padding = 20;
var isRotating = false;
var isFlipped = false;
var fabricImage;
var image = new Image();
// Zoom
var zoomLevel = 0.1;
var zoomLevelMin = -3;
var zoomLevelMax = 3;
var targetZoomLevel = 0.1; // used for lerp
var zoomAmount = 0.4;
var focusPoint = null;

//INIT
init();
function init() {
    //Create canvas wrapper div to put canvas in so I can hide and move it around
    var canvasWrapper = document.createElement('div');
    canvasWrapper.id = 'canvasWrapper';
    canvasWrapper.style.position = "absolute";
    canvasWrapper.style.top = "0px";
    canvasWrapper.style.left = "0px";
    canvasWrapper.style.height = "100%";
    canvasWrapper.style.width = "100%";
    canvasWrapper.style.zIndex = "1000";
    document.body.appendChild(canvasWrapper);

    var canvasElement = document.createElement('canvas');
    canvasElement.id = 'imageCanvas';
    canvasWrapper.appendChild(canvasElement);

    canvas = new fabric.Canvas('imageCanvas');

    canvas.setWidth(window.innerWidth);
    canvas.setHeight(window.innerHeight);
    canvas.selection = false;
    getSavedBackgroundColor( (color) => {
        canvas.setBackgroundColor(color, canvas.renderAll.bind(canvas));
    });

    document.getElementById("canvasWrapper").style.display = "none"
}

// Save/create tab id so I can keep track of which tab has which image. Used for saving image on tab reload
chrome.runtime.sendMessage({"action" : "getURL"}, function(imageURL) {
    chrome.tabs.getCurrent(function(tab){
        if (sessionStorage.getItem("imageURL" + tab.id) == null) {
            image.src = imageURL;
            sessionStorage.setItem("imageURL" + tab.id, imageURL);
        } else {
            image.src = sessionStorage.getItem("imageURL" + tab.id);
        }
    });
});

image.onload = function() {
    function createCanvasImageObject() {
        var imageDimensions = calculateAspectRatioFit(image.width, image.height, canvas.getWidth() - padding, canvas.getHeight() - padding);

        fabricImage = new fabric.Image( image, {
          left: canvas.width/2,
          top: canvas.height/2,
          scaleX: imageDimensions.width / image.width,
          scaleY: imageDimensions.height / image.height,
          angle: imageAngle,
          originX: 'center',
          originY: 'center',
          hoverCursor:'cursor',
          selectable: false
        });

        canvas.add(fabricImage);
    }

    if (fabricImage == null) {
        createCanvasImageObject();
    } else {
        canvas.remove(fabricImage);

        createCanvasImageObject();
    }

    showCanvas();
}

function showCanvas() {
    document.getElementById("canvasWrapper").style.display = "block"
    document.body.style.overflow = "hidden";

    window.addEventListener('resize', function(e) {handleWindowResize(e);});
    document.addEventListener('keyup', function(e) {handleKeyUp(e);});
    $(document).on('keydown', function(options) {handleKeyDown(options);});
    canvas.on('mouse:down', function(options) { handleMouseDown(options); });
    canvas.on('mouse:up', function(options) { handleMouseUp(options); });
    canvas.on('mouse:move', function(options) { handleMouseMove(options) });
    $('.canvas-container').on('mousewheel', function(options) { handleScroll(options); });
    $('.canvas-container').on('contextmenu', function(options) { handleContextMenu(options); });

    //Need to move canvas to current scroll position
    document.getElementById("canvasWrapper").style.top = window.scrollY + "px"; 

    bUpdateCanvas = true;
    resetImage();
    update();
}

function viewImageInCurrentTab(imageSource) {
    isViewingCurrentTab = true;
    image.src = imageSource;
}

function update() {
    // Flipping
    fabricImage.set('flipX', isFlipped); 

    // Rotating
    let a = Math.floor(imageAngle/90);
    fabricImage.set('angle', 90 * a);

    // Zoomming
    zoomLevel += (targetZoomLevel - zoomLevel)*lerpFactor;
    if (focusPoint) {
        canvas.zoomToPoint(focusPoint, Math.pow(2, zoomLevel)); 
    }

    // Panning
    targetDeltaX -= (targetDeltaX - mouseDeltaX)*lerpFactor;
    targetDeltaY -= (targetDeltaY - mouseDeltaY)*lerpFactor;
    canvas.relativePan(new fabric.Point(-targetDeltaX, -targetDeltaY));

    if (currentMousePoint)
    {
        if (isDragging)
        {
            var mouseMovePoint = new fabric.Point(currentMousePoint.x, currentMousePoint.y);

            mouseDeltaX = mouseLastPoint.x - currentMousePoint.x;
            mouseDeltaY = mouseLastPoint.y - currentMousePoint.y;
            mouseLastPoint = mouseMovePoint;
        }
    }
    canvas.renderAll();

    if (bUpdateCanvas) {
        window.requestAnimationFrame(update);
    }
}

function handleMouseMove(options) {
       currentMousePoint = canvas.getPointer(options.e, true);

        if (isRotating) {
            let curATan = getATan(currentMousePoint.x, currentMousePoint.y);
            let delta = (curATan - lastATanAngle);

            imageAngle += delta; 
        }

        lastATanAngle = getATan(currentMousePoint.x, currentMousePoint.y)
}

function handleKeyDown(options){
    switch (options.key) {
        case "Backspace":
            if (isViewingCurrentTab) {
                hideCanvas();
            }
            break;
        case "Escape":
            if (isViewingCurrentTab) {
                hideCanvas();
            }
            break;
        case "ArrowUp":
            isFlipped = !isFlipped;
            break;
        case " ":
            resetImage();
            break;
        case "1": {
            let c = "#FFFFFF";
            canvas.setBackgroundColor(c, canvas.renderAll.bind(canvas));
            saveBackgroundColor(c);
            } break;
        case "2": {
            let c = "#000000";
            canvas.setBackgroundColor(c, canvas.renderAll.bind(canvas));
            saveBackgroundColor(c);
            } break;
        case "ArrowLeft": 
           imageAngle -= 90; 
           if (imageAngle < -360) {
                imageAngle = -90;
            }
           break;
        case "ArrowRight": 
           imageAngle += 90; 
           if (imageAngle > 360) {
                imageAngle = 90;
           }
           break;
       case "r":
            isRotating = true;
            break;
       case "f":
            isFlipped = !isFlipped;
            break;
       case "y": {
                downloadImage();
            }
            break;
    }

    function hideCanvas() {
        document.body.style.overflow = "visible";

        $(document).off("keydown");
        canvas.__eventListeners["mouse:down"] = [];
        canvas.__eventListeners["mouse:move"] = [];
        canvas.__eventListeners["mouse:up"] = [];
        canvas.__eventListeners["mouse:up"] = [];
        $('.canvas-container').off("mousewheel");

        bUpdateCanvas = false;
        document.getElementById("canvasWrapper").style.display = "none"
    }
}

function handleKeyUp(e){
    if (e.code == "KeyR") {
        isRotating = false;
    }
}

function handleMouseDown(options) {
    var currentMousePoint = canvas.getPointer(options.e, true);
    mouseLastPoint = new fabric.Point(currentMousePoint.x, currentMousePoint.y);
    isDragging = true;
}

function handleMouseUp(options) {
    mouseDeltaX = 0;
    mouseDeltaY = 0;
    mouseLastPoint = null;
    isDragging = false;
}

function handleContextMenu(options) {
    options.preventDefault(); //Block context menu because it's useless
}

function handleScroll(options) {
    options.preventDefault();

    var delta = options.originalEvent.wheelDelta;
    if (delta != 0) {
        var currentMousePoint = canvas.getPointer(options.e, true);

        focusPoint = new fabric.Point(currentMousePoint.x, currentMousePoint.y);
        if (delta > 0) {
          zoomIn(focusPoint);
        } else if (delta < 0) {
          zoomOut(focusPoint);
        }
    }
}

function handleWindowResize(e) {
    canvas.setWidth(window.innerWidth);
    canvas.setHeight(window.innerHeight);
    document.getElementById("canvasWrapper").style.top = window.scrollY + "px"; 

    var imageDimensions = calculateAspectRatioFit(image.width, image.height, canvas.getWidth() - padding, canvas.getHeight() - padding);

    fabricImage.set('left', canvas.width/2);
    fabricImage.set('top', canvas.height/2);
    fabricImage.set('scaleX', imageDimensions.width / image.width); 
    fabricImage.set('scaleY', imageDimensions.height / image.height); 

    resetImage(); 
} 

function zoomIn(point) {
  if (zoomLevel < zoomLevelMax) {
    targetZoomLevel = zoomLevel + zoomAmount;
  }
}

function zoomOut(point) {
  if (zoomLevel > zoomLevelMin) {
    targetZoomLevel -= zoomAmount;
  }
}


function resetImage() {
    canvas.setViewportTransform( [1,0,0,1,0,0 ]);
    targetDeltaX = 0;
    targetDeltaY = 0;

    zoomLevel = 0.02;
    targetZoomLevel = 0.02;
    if (focusPoint) {
        canvas.zoomToPoint(focusPoint, Math.pow(2, zoomLevel)); 
    }

    imageAngle = 0;
    isFlipped = false;
}

function downloadImage() {
    //Create temp dummy link that has image as source and click on it
    let imageLink = document.createElement("a");
    imageLink.id = 'downloadImageLink';
    imageLink.href = image.src;
    imageLink.download = "image.png";
    document.body.appendChild(imageLink);

    imageLink.click();
}

function saveBackgroundColor(color) {
  var backgroundColor = "";
  backgroundColor = color; 

  chrome.storage.sync.set({"backgroundColor": backgroundColor});
}

function getSavedBackgroundColor(callback) {
  chrome.storage.sync.get("backgroundColor", function (item) {
    callback(chrome.runtime.lastError ? null : item.backgroundColor);
  });
}

function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

    return { width: srcWidth*ratio, height: srcHeight*ratio };
 }

 function getATan(mouseX, mouseY) {
    return Math.atan2(mouseY - fabricImage.get('top') ,
                      mouseX - fabricImage.get('left')) * (180 / Math.PI) - 90;
}