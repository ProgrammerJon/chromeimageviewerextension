//Init
{ 
    //Add click listeners to links and divs
    var links = document.getElementsByTagName('a');
    for (var i=0, length = links.length; i < length; i++) {
      links[i].addEventListener("click", handleClick, true); 
    }

    var divs = document.getElementsByTagName('div');
    for (var i=0, length = divs.length; i < length; i++) {
      divs[i].addEventListener("click", handleClick, true); 
    }

    var imageElements = document.getElementsByTagName('img');
    for (var i=0, length = imageElements.length; i < length; i++) {

      //Last argument sets useCapture making this event called first or at least close to first
      imageElements[i].addEventListener("click", handleClick, true); 
    }
}

function handleClick(e) {
    if (e.which == 1) { 
        //If shift+alt open new tab with making it active tab else make active
        if (e.shiftKey) {
            if (e.altKey) {
                e.preventDefault(); //Prevents downloading on alt-click
                e.stopPropagation(); //Stop links from working

                var srcElement = e.srcElement;

                // Lets check if our underlying element is a IMG.
                if (srcElement.nodeName == 'IMG') {
                    //Send image url to background script
                    chrome.runtime.sendMessage({"action" : "sendImageURL", imageURL: srcElement.src, active: false}, function(response) { });
                }
            }
        }  
        else if (e.altKey) {
            e.preventDefault(); //Prevents downloading on alt-click
            e.stopPropagation(); //Stop links from working

            var srcElement = e.srcElement;

            // Lets check if our underlying element is a IMG.
            if (srcElement.nodeName == 'IMG') {
                viewImageInCurrentTab(e.srcElement.src);
            }
        }
    }
}

