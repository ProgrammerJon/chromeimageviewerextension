var imageURLString = "no URL set";

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    console.log(sender.tab ?
                "from a content script:" + sender.tab.url :
                "from the extension");

    //Sends the imageURLString to image viewer
	if(request.action == "getURL") {
		sendResponse(imageURLString); // No need to serialize yourself!
	}
	//Get the image url from the content script
	if (request.action == "sendImageURL") {
		imageURLString = request.imageURL; 		 	

		chrome.tabs.create({ url: "imageviewer.html", active: request.active, index: sender.tab.index+1 });
	}
});