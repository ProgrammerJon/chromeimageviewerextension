# What is Chrome Image Viewer? 
----

I'm always opening images for reference by right clicking and selecting "open image in new tab" in the context menu but I find the way this works annoying for two reasons. First, there's no easy shortcut. And second, the controls for zooming and moving around the image is SO clunky. It's probably fine for someone who uses this feature every once in awhile, but for someone who uses it a hundred times a day it's super frustrating. So I created this extension which adds Alt+Click shortcut and a much better viewer.

|  
|  
|  
      
![example gif](https://orig00.deviantart.net/5f00/f/2018/053/6/f/update3_by_doctorcocobean-dc414o2.gif "example gif")

# How do I get this?
----
You can download the `.zip` file [here](https://bitbucket.org/ProgrammerJon/chromeimageviewerextension/get/daab7d6a27ca.zip) and enable Chrome Image Viewer by going to the `chrome://extensions` URL and checking developer mode, then pointing Chrome to the unzipped folder via the Load `unpacked extensions...` button.


# Shortcuts
----
* Alt+Left_Click to open image in current tab over website
* Alt+Shift+Left_Click to open images in new tab with image viewer
* Backspace/Escape to exit/close image viewer if opened in current tab
* Press space to reset image position, rotation, zoom level, etc
* Rotate image with R+Mouse or arrow keys
* F to flip horizontally
* Press 1 and 2 to change background color (1 is better for transparent images)
 
# Known Issues
----
I mostly made this for myself so it's not perfect and I can't garentee it work will 100% of the
time. But for me it's made viewing images on Chrome so much more enjoyable.

Here are some known problems:

* Sometimes when zooming in at certain parts of the image will cause it to disapear. I'm not sure why this happens but it doesn't affect me 99% of the time and simply refreshing (ctrl+r) fixes it for me.

* On some websites (for example Tumblr) viewing images in current tab can be buggy. I mostly tested the extension on Pinterest, Artstation, DeviantArt and Google Images.

* I got the thumbnail image icon off google images which might be problem later on for copyright reasons, but for now since this is a personal tool I'm not going to stress over it.